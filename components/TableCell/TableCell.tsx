import * as React from 'react';
import { View, Text, StyleSheet, Animated } from 'react-native';
import Layouts from '../../constants/Layout';
import { IQuotes } from '../../constants/types';

interface IProps {
  data: IQuotes;
}

export const TableCell: React.SFC<IProps> = props => {
  const [margin, animation] = React.useState(
    new Animated.Value(Layouts.window.width)
  );
  React.useEffect(() => {
    Animated.timing(margin, {
      toValue: 0,
      duration: 500
    }).start();
  });

  return (
    <Animated.View
      style={{
        marginLeft: margin,
        display: 'flex',
        flexDirection: 'row',
        textAlign: 'center'
      }}
    >
      <View style={styles.tableTd}>
        <Text style={styles.text}>{props.data.name} </Text>
      </View>
      <View style={styles.tableTd}>
        <Text style={styles.text}>{props.data.last} </Text>
      </View>
      <View style={styles.tableTd}>
        <Text style={styles.text}>{props.data.percentChange} </Text>
      </View>
      <View style={styles.tableTd}>
        <Text style={styles.text}>{props.data.highestBid} </Text>
      </View>
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  tableCell: {
    display: 'flex',
    flexDirection: 'row',
    textAlign: 'center'
  },
  tableTd: {
    flex: 1,
    paddingVertical: 8,
    borderWidth: 1,
    borderColor: 'grey'
  },
  text: {
    fontSize: 14,
    lineHeight: 18,
    textAlign: 'center'
  }
});
