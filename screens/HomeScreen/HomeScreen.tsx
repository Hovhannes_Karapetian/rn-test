import * as React from 'react';
import { StyleSheet, View, Button } from 'react-native';
import {
  NavigationPushAction,
  NavigationState,
  NavigationScreenProp,
  NavigationScreenProps,
  NavigationScreenComponent
} from 'react-navigation';
import { path } from '../../constants/path';

interface IProps extends NavigationScreenProps {
  navigationOptions?: Object;
}
export const HomeScreen: NavigationScreenComponent<IProps> = ({
  navigation
}) => {
  return (
    <View style={styles.container}>
      <Button
        title='Экран Котировки'
        onPress={() => navigation.navigate(path.quotes)}
      />
    </View>
  );
};
HomeScreen.navigationOptions = { title: 'О приложении' };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingBottom: 14
  }
});
