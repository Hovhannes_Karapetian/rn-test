import * as React from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Button,
  ActivityIndicator
} from 'react-native';
import { TableCell } from '../../components';
import {
  NavigationScreenProp,
  NavigationState,
  NavigationPushAction
} from 'react-navigation';
import { path } from '../../constants/path';
import { IQuotes } from '../../constants/types';

interface IState {
  data?: Array<IQuotes>;
  error: boolean;
}
interface IProps {
  navigation: NavigationScreenProp<NavigationPushAction, NavigationState>;
}

const tableHead = {
  name: 'Name',
  last: 'last',
  highestBid: 'High',
  percentChange: 'percent'
};
export default class QuotesScreen extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      error: false
    };
  }
  timer: any;

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: (
        <View style={styles.titleHeader}>
          <Text style={styles.headerText}>{'Котировки'}</Text>
          <Button
            onPress={() => navigation.navigate(path.home)}
            title='О приложении'
          />
        </View>
      )
    };
  };

  componentDidMount() {
    this.timer = setInterval(() => this.getData(), 5000);
  }
  componentDidUpdate() {
    if (!this.props.navigation.isFocused()) {
      clearInterval(this.timer);
    }
  }
  getData = () => {
    fetch(path.api)
      .then(response => {
        return response.json();
      })
      .then(myJson => {
        let keys = Object.keys(myJson);
        let data = keys.map((item, index) => {
          let dataItem = myJson[item];
          dataItem.name = item;
          return dataItem;
        });
        if (this.state.data !== data) {
          this.setState({ data: data, error: false });
        }
      })
      .catch(e => {
        console.warn(e);
        this.setState({ error: true, data: null });
      });
  };
  render() {
    const { data, error } = this.state;
    return (
      <View style={styles.container}>
        <View />
        <View style={styles.tableHead}>
          <TableCell data={tableHead} />
        </View>
        {data ? (
          <ScrollView>
            {data.map((item: IQuotes, index) => (
              <TableCell data={item} key={index} />
            ))}
          </ScrollView>
        ) : (
          <View style={styles.indicatorContainer}>
            {error ? (
              <Text style={styles.errorMessages}>{'Ошибка'}</Text>
            ) : (
              <ActivityIndicator size='large' color='#0000ff' />
            )}
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  titleHeader: {
    flex: 1,
    flexDirection: 'row',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 8,
  },
  headerText: {
    fontSize: 20,
    fontWeight: '600'
  },
  container: {
    marginTop: 4,
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    paddingHorizontal: 4
  },
  tableHead: {
    elevation: 3,
    marginBottom: 12
  },
  indicatorContainer: {
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  errorMessages: {
    color: 'red',
    fontSize: 22
  }
});
