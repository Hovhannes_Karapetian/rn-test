export interface IQuotes {
    name: string;
    last: string | number;
    highestBid: string | number;
    percentChange: string | number;
  }