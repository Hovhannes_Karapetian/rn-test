import React from 'react';
import { Platform } from 'react-native';
import {
  createStackNavigator,
  createBottomTabNavigator
} from 'react-navigation';

import { TabBarIcon } from '../components';
import { HomeScreen, QuotesScreen } from '../screens';
import { path } from '../constants/path';

const HomeStack = createStackNavigator({
  [path.home]: HomeScreen
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Главная',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  )
};

const QuotesStack = createStackNavigator({
  [path.quotes]: QuotesScreen
});

QuotesStack.navigationOptions = {
  tabBarLabel: 'Котировки',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'}
    />
  )
};

const tabNavigator = createBottomTabNavigator({
  HomeStack,
  QuotesStack
});

export default tabNavigator;
