import * as React from 'react';
import { Ionicons } from '@expo/vector-icons';

import Colors from '../../constants/Colors';

interface IProps {
  name: string;
  focused?: boolean;
}

const TabBarIcon: React.SFC<IProps> = props => {
  return (
    <Ionicons
      name={props.name}
      size={26}
      style={{ marginBottom: -3 }}
      color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
    />
  );
};

export default TabBarIcon;
